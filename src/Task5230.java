import java.util.ArrayList;

public class Task5230 {
    public static void main(String[] args) throws Exception {
        ArrayList<Person> arrayListPersons = new ArrayList<>();
        //task 2
        //khởi tạo 4 person với tham số khác nhau
        Person person0 = new Person();
        Person person1 = new Person("Trang",30,48,50000000, new String[] {"dog"});
        Person person2 = new Person("Minh");
        Person person3 = new Person("Hà",28,50);
        //cho vào array list
        arrayListPersons.add(person0);
        arrayListPersons.add(person1);
        arrayListPersons.add(person2);
        arrayListPersons.add(person3);
        //task 3
        for (int i=0; i<arrayListPersons.size();i++){
            System.out.println(arrayListPersons.get(i));
        }
    }
}
