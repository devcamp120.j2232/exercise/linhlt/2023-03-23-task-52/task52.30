import java.util.Arrays;

//subtask 1
public class Person {
    @Override
    public String toString() {
        return "Person [name=" + name + ", age=" + age + ", weight=" + weight + ", salary=" + salary + ", pets="
                + Arrays.toString(pets) + "]";
    }
    String name;
    int age;
    double weight;
    long salary;
    String[] pets;

    //khởi tạo 1 tham số name
    public Person(String name) {
        this.name = name;
        this.age = 40;
        this.weight = 70;
        this.salary = 10000000;
    }
    //khởi tạo với tất cả tham số
    public Person(String name, int age, double weight, long salary, String[] pets) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.salary = salary;
        this.pets = pets;
    }
    //khởi tạo không tham số
    public Person() {
    }
    //khởi tạo với 3 tham số
    public Person(String name, int age, double weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
        this.salary = 15000000;
    }
    
     
}

